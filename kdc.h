#pragma once
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <random>
#include <ctime>
#include "des.h"
#include "ss.h"


class KDC {
private:
    std::map<std::string, uint64_t> db;
    std::string tgs;    
    uint64_t Ktgs_as;
    std::map<std::string, uint64_t> Kss;    
    uint64_t gen_key();
public:
    KDC(std::string tgs);
    bool sign_up(std::string login, std::string password);
    bool register_server(SS& ss);
    std::hash<std::string> hash_fn;
    std::string send_tgt(std::string c);
    std::string send_tgs(std::string* msg);
};