#include "kdc.h"


KDC::KDC(std::string tgs) {
    this->tgs = tgs;
    Ktgs_as = gen_key();    
    std::fstream fs("db.txt");
    std::stringstream ss;
    std::string line, login, password;

    while(std::getline(fs, line, '\n')) {
        ss << line;
        ss >> login >> password;
        db[login] = hash_fn(password);
    }

}

bool KDC::sign_up(std::string login, std::string password) {
    if(db.count(login) > 0) {
        std::cout << "user already exist." << std::endl;
        
        return false;
    }

    db[login] = hash_fn(password);
    std::ofstream ofs;
    ofs.open("db.txt", std::ios::app);
    ofs << login << " " << password << std::endl;
    ofs.close();

    return true;
}

uint64_t KDC::gen_key() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<uint64_t> dis;
    
    return dis(gen);
}

bool KDC::register_server(SS& ss) {
    if(Kss.count(ss.get_ss()) > 0) {
        std::cout << "server elready esist." << std::endl;

        return false;
    }

    Kss[ss.get_ss()] = gen_key();
    ss.set_Kss_tgs(Kss[ss.get_ss()]);

    return true;
}

std::string KDC::send_tgt(std::string c) {
    std::cout << "AS receive {c}." << std::endl;    //logging

    if(db.count(c) == 0) {
        std::cout << "no such user here." << std::endl;
        
        exit(0);
    }

    uint64_t Ktgs_c = gen_key();

    std::string msg = c + "\n" + tgs + "\n";
    time_t t1 = time(NULL);
    time_t p1 = 36000LL;
    msg += std::to_string(t1) + "\n" + std::to_string(p1) + "\n" + std::to_string(Ktgs_c);
    msg = strdes(msg, Ktgs_as, false);

    msg += "\n" + std::to_string(Ktgs_c);
    msg = strdes(msg, db[c], false);

    std::cout << "AS send {{TGT}Kas_tgs, Kc_tgs}Kc." << std::endl;  //logging

    return msg;
}

std::string KDC::send_tgs(std::string* msg) {
    std::cout << "TGS receive {TGT}Kas_tgs, {Aut1} Kc_tgs, {ID}." << std::endl;   //logging

    std::string str;
    std::stringstream ss(strdes(msg[0], Ktgs_as, true));
    std::string tgt_c, tgt_tgs;
    time_t t1, p1;
    uint64_t Kc_tgs;
    std::getline(ss, tgt_c, '\n');
    std::getline(ss, tgt_tgs, '\n');
    std::getline(ss, str, '\n');
    t1 = std::stoll(str);
    std::getline(ss, str, '\n');
    p1 = std::stoll(str);
    std::getline(ss, str, '\n');
    Kc_tgs = std::stoull(str);
    
    ss.clear();
    ss.str(strdes(msg[1], Kc_tgs, true));
    std::string aut1_c;
    time_t t2;
    std::getline(ss, aut1_c, '\n');
    std::getline(ss, str, '\n');
    t2 = std::stoll(str);
    
    if(!((tgt_c.compare(aut1_c) == 0) && (tgs.compare(tgt_tgs) == 0) && (t2 >= t1) && ((t1 + p1) > t2))) {
        std::cout << "1'st autentication phase failed." << std::endl;
        
        exit(0);
    }

    if(Kss.count(msg[2]) == 0) {
        std::cout << "no such server here." << std::endl;
        
        exit(0);
    }

    uint64_t Kc_ss = gen_key();
    str = tgt_c + "\n" + msg[2] + "\n";
    time_t t3 = time(NULL);
    time_t p2 = 3600LL;
    str += std::to_string(t3) + "\n" + std::to_string(p2) + "\n" + std::to_string(Kc_ss);
    str = strdes(str, Kss[msg[2]], false);

    str += "\n" + std::to_string(Kc_ss);
    str = strdes(str, Kc_tgs, false);

    std::cout << "TGS send {{TGS}Ktgs_ss,Kc_ss}Kc_tgs." << std::endl;  //logging

    return str;
}