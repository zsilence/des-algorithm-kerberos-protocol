#include <iostream>
#include <string>
#include "kdc.h"
#include "client.h"
#include "ss.h"


int main() {
    KDC kdc = KDC("tgs");
    Client client;
    SS ss;

    for(;;) {
        char c;
        std::string name, password;
        std::cout << "sign in or sign up[i/u]: ";
        std::cin >> c;
        if(c == 'i') {
            std::cout << "enter user name: ";
            std::cin >> name;
            std::cout << "enter password: ";
            std::cin >> password;
            client = Client(name, kdc.hash_fn(password));  
            
            break;  
        } else if(c == 'u') {
            do {
                std::cout << "enter user name: ";
                std::cin >> name;
                std::cout << "enter password: ";
                std::cin >> password;
            } while(!kdc.sign_up(name, password));
            client = Client(name, kdc.hash_fn(password));        
            
            break;
        }
    }

    std::string server_id;
    std::cout << "enter server id: ";
    std::cin >> server_id;
    ss = SS(server_id);
    if(!kdc.register_server(ss)) return 0;

    client.receive_tgt(kdc.send_tgt(client.get_c()));

    client.receive_tgs(kdc.send_tgs(client.send_tgt(ss.get_ss())));

    if(client.confirm_ss(ss.receive_tgs(client.send_tgs(ss.get_ss())))) {
        std::cout << "authentication successful." << std::endl;
    } else {
        std::cout << "authentication failed." << std::endl;
    }

    return 0;
}