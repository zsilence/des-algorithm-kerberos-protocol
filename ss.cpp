#include "ss.h"


SS::SS() {
    this->ss = "ss";
}

SS::SS(std::string ss) {
    this->ss = ss;

}

std::string SS::get_ss() {
    
    return ss;
}

void SS::set_Kss_tgs(uint64_t Kss_tgs) {
    this->Kss_tgs = Kss_tgs;

}

std::string SS::receive_tgs(std::string* msg) {
    std::cout << "SS receive {{TGS}Ktgs_ss,Kc_ss}Kc_tgs." << std::endl; //logging

    std::string str;
    std::stringstream ss(strdes(msg[0], Kss_tgs, true));
    std::string tgs_c, tgs_ss;
    time_t t3, p2;
    std::getline(ss, tgs_c, '\n');
    std::getline(ss, tgs_ss, '\n');
    std::getline(ss, str, '\n');
    t3 = std::stoll(str);
    std::getline(ss, str, '\n');
    p2 = std::stoll(str);
    std::getline(ss, str, '\n');
    Kss_c = std::stoull(str);
    
    ss.clear();
    ss.str(strdes(msg[1], Kss_c, true));
    std::string aut2_c;
    time_t t4;
    std::getline(ss, aut2_c, '\n');
    std::getline(ss, str, '\n');
    t4 = std::stoll(str);
   
    if(!((tgs_c.compare(aut2_c) == 0) && (this->ss.compare(tgs_ss) == 0) && (t4 >= t3) && ((t3 + p2) > t4))) {
        std::cout << "2'nd autentication phase failed." << std::endl;
        
        exit(0);
    }

    std::cout << "SS send {t4+1}Kc_ss." << std::endl;   //logging

    return strdes(std::to_string(t4 + 1LL), Kss_c, false);
}