#pragma once
#include <string>
#include <sstream>
#include <iostream>
#include "des.h"


class SS {
private:
    std::string ss;
    uint64_t Kss_c;
    uint64_t Kss_tgs;
public:
    SS();
    SS(std::string ss);
    std::string get_ss();
    void set_Kss_tgs(uint64_t Kss_tgs);
    std::string receive_tgs(std::string* msg);
};