#include "client.h"


Client::Client() {
    this->c = "client";
    this->Kc = 0xC11E2;
}

Client::Client(std::string c, uint64_t Kc) {
    this->c = c;
    this->Kc = Kc;

}

std::string Client::get_c() {
    std::cout << "C send {c}." << std::endl;    //logging

    return c;
}

void Client::receive_tgt(std::string msg) {
    std::cout << "C receive {{TGT}Kas_tgs, Kc_tgs}Kc." << std::endl;   //logging

    msg = strdes(msg, Kc, true);

    size_t i, len = msg.length() - 1ULL;
    for(i = len; msg[i] != '\n'; --i);
    Kc_tgs = std::stoull(std::string(msg, i + 1ULL));
    
    TGT = std::string(msg, 0, i);

}

std::string* Client::send_tgt(std::string ss) {
    time_t t2 = time(NULL);
    std::string aut1 = strdes(c + "\n" + std::to_string(t2), Kc_tgs, false);

    std::cout << "C send {TGT}Kas_tgs, {Aut1} Kc_tgs, {ID}." << std::endl;  //logging

    return new std::string[3] {TGT, aut1, ss};
}

void Client::receive_tgs(std::string msg) {
    std::cout << "C receive {{TGS}Ktgs_ss,Kc_ss}Kc_tgs." << std::endl;   //logging

    msg = strdes(msg, Kc_tgs, true);

    size_t i, len = msg.length() - 1ULL;
    for(i = len; msg[i] != '\n'; --i);
    Kc_ss = std::stoull(std::string(msg, i + 1ULL));
    
    TGS = std::string(msg, 0, i);

}

std::string* Client::send_tgs(std::string ss) {
    t4 = time(NULL);
    std::string aut2 = strdes(c + "\n" + std::to_string(t4), Kc_ss, false);

    std::cout << "C send {TGS}Ktgs_ss, {Aut2} Kc_ss." << std::endl;   //logging

    return new std::string[2] {TGS, aut2};
}

bool Client::confirm_ss(std::string msg) {
    std::cout << "C receive {t4+1}Kc_ss." << std::endl; //logging

    msg = strdes(msg, Kc_ss, true);

    return ((t4 + 1LL) == std::stoll(msg));
}