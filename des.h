#pragma once
#include <string>


uint64_t* des(uint64_t* bits, size_t n, uint64_t K, bool decrypt);
std::string strdes(std::string str, uint64_t K, bool decrypt);