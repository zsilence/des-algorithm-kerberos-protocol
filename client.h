#pragma once
#include <string>
#include <iostream>
#include <ctime>
#include "des.h"


class Client {
private:
    std::string c;
    std::string TGT;   
    std::string TGS;   
    uint64_t Kc;
    uint64_t Kc_tgs;
    uint64_t Kc_ss;
    time_t t4;
public:
    Client();
    Client(std::string c, uint64_t Kc);
    std::string get_c();
    void receive_tgt(std::string msg);
    std::string* send_tgt(std::string ss);
    void receive_tgs(std::string msg);
    std::string* send_tgs(std::string ss);
    bool confirm_ss(std::string msg);
};